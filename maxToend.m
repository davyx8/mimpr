function seg= maxToend(im, maxidx,segmented,idx )
CC=bwconncomp(im(:,:,maxidx));  
[x y z] = size(im);
maxSize=numel(CC.PixelIdxList{idx});
itrSize=numel(CC.PixelIdxList{idx});
prevSlice=zeros(size(im(:,:,1)));
prevSlice(CC.PixelIdxList{idx})=1;
prevItrsize=itrSize;
segmented(:,:,maxidx)=prevSlice;
stats=regionprops(prevSlice,'Centroid');
itrLocation=stats.Centroid;
prevLocation=itrLocation;
for j=drange(maxidx+1:z-1)
    idxslice=1;
    minDiff=intmax('int64');
    CC=bwconncomp(im(:,:,j));
    minSizeDiff=intmax('int64');
    tmpSlice=zeros(size(im(:,:,1)));
    for i=drange(1:numel(CC.PixelIdxList))
        tmpSlice(CC.PixelIdxList{i})=1;
        stats=regionprops(tmpSlice,'Centroid');
        currLocation=stats.Centroid;
        sizeDiff=abs(numel(CC.PixelIdxList{i})-prevItrsize);
        diff=abs(currLocation(1)-prevLocation(1))+abs(currLocation(1)-prevLocation(1));
        if(...
                (sum(bitand(prevSlice,tmpSlice)))>20 &...
                numel(CC.PixelIdxList{i})>1000)

        minDiff=diff;
        minSizeDiff=sizeDiff;
        idxslice=i;
        end
    end
    if(~isempty(CC.PixelIdxList))
        newSlice=zeros(size(im(:,:,1)));
        newSlice(CC.PixelIdxList{idxslice})=1;
        itrSize=numel(CC.PixelIdxList{idxslice});
        itrSize=numel(CC.PixelIdxList{idxslice});
        newSlice(CC.PixelIdxList{idxslice})=1;
    if (itrSize>1.7*prevItrsize)
       % display(j)
        newSlice=bitand(segmented(:,:,j-1),im(:,:,j));
        newSlice =bwmorph(newSlice,'thicken');
        
        %newSlice=bitand(newSlice,im(:,:,j));
    end 
    else
        newSlice=prevSlice;
    end
    if (sum(sum(bitand(prevSlice,newSlice)))<20)  
        sum(sum(bitand(prevSlice,newSlice)));
        newSlice=bitand(im(:,:,j-1),prevSlice);
    end
    itrSize=sum(sum(newSlice));
    segmented(:,:,j)=newSlice;
    
    stats=regionprops(newSlice,'Centroid');
    if (~isempty(stats))
    itrLocation=stats.Centroid;
    prevLocation=itrLocation;
    end
    prevSlice=newSlice;
    prevItrsize=itrSize;
end
seg=segmented;
end