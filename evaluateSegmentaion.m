%This function gets manual segmentation and automatic segmentation and 
%calculates the error values
function [VD,VDO,SD_max,SD_mean,SD_sd ]=evaluateSegmentaion(segmentationFilename,groundTruthFilename)
    SD_max=0;
    SD_mean=0;
    SD_sd=0;

    %loading images
     my_f1 = load_untouch_nii(segmentationFilename);
     my_f2 = load_untouch_nii(groundTruthFilename);
     
     img = double(my_f1.img);
     img1 = img;
     img1 = permute(img1,[2 3 1]);
     
     img = double(my_f2.img);
     img2 = img;
     img2 = permute(img2,[2 3 1]);
     
     %finding sum of both images
     sum1 = sum(sum(sum(img1)));%automatic segmentation size
     sum2 = sum(sum(sum(img2)));%manual segmentation size
     
     %calculating Volume Difference
     
         VD = abs(sum1 - sum2);
     
     %Cropping of images
     img3 = img1&img2;
     sum3=sum(sum(sum(img3)));
     %calculating Volume Overlap Difference
     SIM = (2*sum3)/(sum1+sum2);
     VDO = (1-SIM)*100;
     
%      %Finding derived
%     edge1 = diff(img1);
%     edge2 = diff(img2);
%     
%     %creating matrices including the coordinate values of the limits of the kidney
%      [y1,z1,x1] = ind2sub(size(edge1),find(edge1 == -1));
%      [y2,z2,x2] = ind2sub(size(edge2),find(edge2 == -1));
%      minDist=100000;
%      array1 =  [y1,z1,x1];
%      array2 = [y2,z2,x2];
%      %cheeking which segmentation has more points
%      if( length(array1)> length(array2))
%          len=length(array1);
%      else
%          len=length(array2);
%      end
%      distanceArr=zeros(len,1);
%      % loop on the points that are within the kidney in order 
%      %to compare the distances of the points in both segmentation        
%      for i=1:length(array1)
%          for j=1:length(array2)
%              %cheeking distance between the points
%              distance = [array1(i,1) array1(i,2) array1(i,3); array1(j,1) array1(j,2) array1(j,3) ];
%              tempMinDist=pdist(distance);
%              
%              if tempMinDist<minDist
%                 minDist =tempMinDist;
%              end
%          end
%         
%          distanceArr(i,1)=minDist;
%          minDist=100000;
%    
%      end
%          
%     %maximum Surface Distance
%     SD_max= max(distanceArr);
%     %meam Surface Distance
%     SD_mean=mean(distanceArr);
%     %standarts deviation Surface Distance
%     SD_sd=std2(distanceArr);
                 
end