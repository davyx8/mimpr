function SegmentOrgan(inputFileName,organ_type,outputFileName)
mat=load_untouch_nii_gzip(inputFileName);
im=mat.img;
im=mat2gray(im);

sizes=min(im(:)):0.001:max(im(:));
[o b]=histc(im(:) ,sizes);
[s idx]=max(o(:));
[w h]=size(o);
for i=drange(idx:w)
 if (o(i))<(numel(im)/1000)   
 i_max=sizes(i);
 break;
 end
end
i_min=min(im(:))
mat.img=im;
coarse=findByThreshold(mat, i_max,i_min,0);
SSRG(mat,coarse,organ_type,outputFileName);
end
